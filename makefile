# ---------- ---------- ---------- ---------- ---------- ----------
#   This makefile generates the langskip.
#   
#   char \t:'	'
#   
# ---------- ---------- ---------- ---------- ---------- ----------

FILE=drekis


target: prerequisites 
prerequisites: build

# ---------- ---------- ---------- ---------- ---------- ----------
#NOTE: The recipes follow:
# ---------- ---------- ---------- ---------- ---------- ----------

.PHONY: build
build: 
	./make/build.sh

.PHONY: clean
clean:
	./make/clean.sh output

.PHONE: server
server:
	cd ./output && python3 -m http.server

