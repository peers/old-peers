PEERS LICENSE
=============  

# Peers, itself #
## Code, markup, style ##
Copyright 2015 vaeringjar  

Peers code, markup, and style files licensed under the AGPL, version 3.0 or later (the "License");  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at  

    https://www.gnu.org/licenses/agpl.txt

Unless required by applicable law or agreed to in writing,  
software distributed under this License is distributed on an "AS IS" BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions  
and limitations under the License.  

## Media ##
Peers media files and plain textual information contained in the markup have the CC BY-SA 4.0 license.  
Official legal version:  
https://creativecommons.org/licenses/by-sa/4.0/legalcode

Human readable interpretation version:  
https://creativecommons.org/licenses/by-sa/4.0/  

# Peers projects #
Please note that each of the projects that Peers links to has their own license and policies.  

