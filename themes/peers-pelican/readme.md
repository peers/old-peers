# responsive-pelican: the troll fork

Fork of [Responsive Pelican](https://github.com/ir193/Responsive-Pelican.git), a [pelican](https://github.com/getpelican/pelican) theme. Emphasis on removing tracking elements and SaaSS, making clean links, and both trolls and troll hunting.  

# Acknowledgements

Forked from Responsive Pelican [commit a74606061d62be0f8508ca840375abe52ae24786](https://github.com/ir193/Responsive-Pelican/commit/a74606061d62be0f8508ca840375abe52ae24786) which came from wordpress theme [Responsive](http://wordpress.org/themes/responsive).  

# Screenshot

  ![Screenshot](screenshot.PNG)
