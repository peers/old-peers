# About
This is the official repository of [Peers](http://peers.community) website.  

# Peers
The Peers Community provides a list of a confederation of independent projects that all strive for free (as in freedom!) software and culture.  

You can talk to us on [freepost](http://freepo.st/community/Peers), or at #freepost on freenode.  

# License
Copyright 2015 - 2017 vaeringjar  
Code distributed under the AGPLv3+.  
Media distributed under the CC BY-SA 4.0.  
> *See the LICENSE.md file for more details...*  

# Building
_Work in progress..._

# Localhost Testing

For example, use python's built-in web server:  

    python3 -m http.server

# Notable Legacy

See commit 46c1f63207103c33f3c075b1ca193a3ca5935e39 for the final appearance of the pre-ssg version of the site.  
