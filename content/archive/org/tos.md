Title: Terms of Service
Date: 2017-07-19 00:00
Modified: 2017-07-19 00:00
Category: Org
Tags: org
Slug: tos
Authors: vaeringjar
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: Terms of Service of The Peers Community

# Terms of Service

The Peers Community only provides information about other projects
for the general public.  

# Membership

To join, a member, project, or service must allow for the practice of
freedom for users.  

Members, member projects, and projects of The Peers Community will
receive help on a case-by-case basis, as needed, or when available.  

No member has a direct obligation to help any other member or project.
However, members should notify the rest of the community upon any significant
change to a project that includes changes such as those in ownership,
stewardship, or the decision to discontinue support.  

# Copyright

The Peers Community does not claim any ownership of any data uploaded
by its users. Becoming a member of The Peers Community does not
constitute a transfer of copyrights to The Peers Community.  

# Jurisdiction

The Peers Community servers are hosted on rented servers in the United States.  

