Title: Why?
Date: 2017-07-19 00:00
Modified: 2017-07-19 00:00
Category: Page
Tags: why
Slug: why
Authors: vaeringjar
Description: Welcome to the The Peers Community, a highly motivated community of people with a strong interest in free software and free culture.
Summary: Why?

<section title="Reasons">
    <h1>
        <a id="reasons" name="reasons" style="margin-top:-50px; padding-bottom:50px; display:block;"></a>
        Reasons
    </h1>

    <p>We have many reasons, in general, why to use free software and to use decentralized, private/anonymous services, instead of proprietary software or services.</p>


    <p><em>Note we have only just started working on this. If you would like to contribute to the current discussion, leave a message </em><a href="https://freepo.st/post/fhsco8sn">at freepost</a>.</p>

    <p><strong>Ethical reason:</strong> we all benefit by sharing knowledge</p>
    <p><strong>Personal reason:</strong> with free software, our computers do what we want them to do! On the other hand, with proprietary software computers are controlled by other people.</p>
    <p><strong>Economic reason:</strong> users lock-in. Switching away from proprietary software can require significant time or financial costs.</p>
    <p><strong>Convenience:</strong> forced to do what a company thinks best (for example you must use their software or files type)</p>
    <p><strong>Privacy:</strong> you become constantly watched by somebody who can do pretty much whatever they want with your personal data, because you've "agreed" with the "license agreement" that they can. This is by very far the most serious argument that people seem not to understand, because they always say "so what? What I'm doing with their service is of little value anyway!".</p>
</section>

<section title="Testamonials">
    <h2>Testimonials and Examples</h2>
    <p>We searched the web for Non-Peers Community people who had shared their own experiences and expertise.</p>
    <ul>
        <li>Peter Swire testifies before the Senate Judiciary Committee on <a href="https://fpf.org/wp-content/uploads/2015/07/SwireCrypto070715.pdf">encryption and the balance between public safety and privacy</a>.</li>
        <li>Drew DeVault explains why we should <a href="https://drewdevault.com/2015/11/01/Please-stop-using-slack.html">use IRC instead of slack</a>.</li>
        <li>Alberto of flosspirit went <a href="https://flosspirit.wordpress.com/2015/03/07/1-ano-sin-whatsapp/">one year without Whatsapp</a> (en español).</li>
        <li>Whatsapp blocks users from Telegram from <a href="https://orat.io/blog/as-of-today-whatsapp-is-blocking-telegram-links/">communicating with each other</a>.</li>
        <li>The FSF promotes free software awareness in their video <a href="https://www.fsf.org/blogs/community/user-liberation-watch-and-share-our-new-video">User Liberation</a>.</li>
        <li>Robin Doherty explains <a href="http://robindoherty.com/2016/01/06/nothing-to-hide.html">Why privacy is important, and having &quot;nothing to hide&quot; is irrelevant</a>.</li>
        <li>Erik Möller discusses licensing in <a href="http://freedomdefined.org/Licenses/NC">The case for Free use: reasons not to use a Creative Commons -NC license</a>.</li>
        <li>Dr. Tarek Loubani speaks out how <a href="https://media.ccc.de/v/32c3-7246-free_software_and_hardware_bring_national_sovereignty">free software and hardware become essential to sovereignty among developing nations</a>, and how we can use them to secure infrastructure and information against sophisticated adversaries.</li>
        <li>Actor, comedian, and technologist Stephen Fry summaries the <a href="https://gnu.org/fry/">early history of GNU and Linux and the pragmatic reasons for using free software</a>.</li>
        <li>Minifree maintains their own <a href="https://minifree.org/paypal/">list of how and why proprietary payment services restrict user freedoms</a>.</li>
        <li>PRISM Break maintains a <a href="https://prism-break.org/">website to protect users from insecure internet usage</a>, specifically related to privacy, citing only free software and with all website <a href="https://github.com/nylira/prism-break/blob/master/LICENSE.md">licensed under the GPL</a>.</li>
    </ul>
    
</section>

<section title="Recommendations">

    <h1><a id="recommendations" name="recommendations" style="margin-top:-50px; padding-bottom:50px; display:block;"></a>Recommendations</h1>

    <p>As an alternative to some proprietary communications, and gaming platforms, we recommend the following. For a catalogue of free software in general, we recommend <a href="https://directory.fsf.org/wiki/Main_Page">The Free Software Directory</a>. The FSF also has their own list of recommendations for <a href="https://directory.fsf.org/wiki/Collection:PRISM">secure and private computing</a>. For choosing a free system, GNU recommends a selection of <a href="https://www.gnu.org/distros/free-distros.html">free distros</a>. Also, note that <a href="https://www.gnu.org/links/companies.html">GNU</a> and <a href="http://blog.replicant.us/2015/12/shops-selling-devices-pre-installed-with-replicant/">Replicant</a> both have hardware supplier recommendations.</p>

    <table class="table">
        <tbody>
            <tr>
                <td><a href="https://www.arduino.cc/">Arduino/Genuino</a></td>
                <td></td>
                <td>Libre prototyping platform based on easy-to-use hardware and software (<a href="https://www.arduino.cc/en/Main/FAQ">FAQ</a>)</td>
            </tr>


            <tr>
                <td><a href="https://f-droid.org/">F-Droid</a></td>
                <td><a href="https://gitlab.com/fdroid/fdroidclient">source</a></td>
                <td>Android application package manager</td>
            </tr>


            <tr>
                <td><a href="https://www.mozilla.org/firefox/">Firefox</a></td>
                <td><a href="https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Source_Code/Downloading_Source_Archives">source</a></td>
                <td>Free web browser by Mozilla</td>
            </tr>


            <tr>
                <td><a href="https://freedomboxfoundation.org/">FreedomBox</a></td>
                <td></td>
                <td>A personal server that protects privacy. Also, the project maintains a <a href="https://wiki.debian.org/FreedomBox/Hardware">list of free hardware</a> over at the Debian wiki.</td>
            </tr>


            <tr>
                <td><a href="http://www.gimp.org/">GIMP</a></td>
                <td><a href="http://www.gimp.org/downloads/">source</a></td>
                <td>cross-platform image editor</td>
            </tr>


            <tr>
                <td><a href="http://gnu.io/social/">gnusocial</a></td>
                <td><a href="http://gnu.io/social/resources/code/">source</a></td>
                <td>Federated social networking/communication software for both public and private communications.</td>
            </tr>


            <tr>
                <td><a href="https://www.gnu.org/software/gnuzilla/">IceCat</a></td>
                <td><a href="http://git.savannah.gnu.org/cgit/gnuzilla.git">source</a></td>
                <td>GNU version of the Firefox browser. Also, with the FSF directory list of <a href="https://directory.fsf.org/wiki/IceCat">free addons</a>.</td>
            </tr>


            <tr>
                <td><a href="http://keepass.info/">keepass2</a></td>
                <td><a href="http://sourceforge.net/p/keepass/code/">svn</a></td><!--<a href="https://github.com/cappert/keepass2">unofficial git mirror</a>-->
                <td>A light-weight and easy-to-use password manager.</td>
            </tr>


            <tr>
                <td><a href="http://www.kontalk.org/">Kontalk</a></td>
                <td><a href="https://github.com/kontalk">source</a></td>
                <td>End to end encrypted chat, based on XMPP</td>
            </tr>


            <tr>
                <td><a href="https://libregamewiki.org/">Libregamewiki</a></td>
                <td></td>
                <td>A wiki of free games and related topics by Han Dao. All documentation under dual GNU FDLv1.2 and CC-BY 3.0. Powered by <a href="https://www.mediawiki.org/">MediaWiki</a></td>
            </tr>


            <tr>
                <td><a href="https://www.libreoffice.org/">LibreOffice</a></td>
                <td><a href="https://www.libreoffice.org/download/">source</a></td>
                <td>A powerful office suite, including word processor, spreadsheets, and slidedeck maker.</td>
            </tr>


            <tr>
                <td><a href="http://www.minetest.net/">Minetest</a></td>
                <td><a href="https://github.com/minetest/minetest">source</a></td>
                <td>An infinite-world block sandbox game with survival and crafting.</td>
            </tr>


            <tr>
                <td><a href="http://www.mumble.info/">Mumble</a></td>
                <td><a href="https://github.com/mumble-voip/mumble">source</a></td>
                <td>A voicechat (VoIP) program originally implemented for gamers written on top of Qt and Speex.</td>
            </tr>


            <tr>
                <td><a href="http://www.openphoenux.org/">OpenPhoenux</a></td>
                <td></td>
                <td>Independent Mobile Tool Community, whose devices run free software.</td>
            </tr>


            <tr>
                <td><a href="http://www.replicant.us/">Replicant</a></td>
                <td><a href="https://git.replicant.us/">source</a></td>
                <td>A free fork of Android.</td>
            </tr>


            <tr>
                <td><a href="https://www.videolan.org/vlc/">VLC Media Player</a></td>
                <td><a href="https://www.videolan.org/vlc/download-sources.html">source</a></td>
                <td>A cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols.</td>
            </tr>

        </tbody>
    </table>

</section>

<footer>
    <small>&copy; 2017 Peers</small>
    <small>&bull; Website Code distributed under the AGPLv3+</small>
    <small>&bull; Website Media licensed under CC BY-SA 4.0</small>
</footer>

